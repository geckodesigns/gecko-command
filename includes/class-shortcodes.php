<?php
/**
 * GeckoCommand shortcode class.
 *
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */

namespace Gecko\Command; 


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Shortcodes.
 */
class Shortcodes {
	
	/*
	 * Register Shortcodes : Need a way to auto load shortcodes and then a filter so more can be registered.
	 */
	public function __construct() {
		$this->base_shortcodes(); // Default Shortcodes
		add_action('media_buttons', array($this , 'media_buttons')); //Media Buttons
		add_filter( 'term_description', 'do_shortcode' ); // Make sure term_description can have shortcodes might have to add more of these.
	}

	/*
	 * Returns an array of available shortcodes
	 */
	public static function get_shortcodes(){
		$shortcodes = array();
		$shortcodes = apply_filters( 'gecko/register/shortcodes', $shortcodes ); 
		return $shortcodes;
	}

	/*
	 * ShortCode Media Button
	 */
	public function media_buttons(){
		wp_enqueue_script('gecko-media-buttons', GECKOCOMMAND__PLUGIN_URL.'/assets/js/media-buttons.js', ['jquery'], GECKOCOMMAND__VERSION, true);
		wp_enqueue_style('gecko-media-buttons', GECKOCOMMAND__PLUGIN_URL.'/assets/css/media-buttons.css', [], GECKOCOMMAND__VERSION);
		$shortcodes = $this::get_shortcodes();
		echo '<span class="gecko-shortcodes-wrapper">';
		echo 	'<a href="#" class="button"><span class="gecko-shortcodes__icon"></span>Shortcodes</a>';
		echo 	'<ul>';
		foreach ($shortcodes as $key => $info) {
			echo '<li data-gecko-shortcode=\'['.$key;
			foreach ($info['attributes'] as $att => $value) {
				echo ' '.$att.'="'.$value.'"';
			}
			if($info['supports_content']){
				echo ']'.$info['sample_content'].'[/'.$key.']';
			}else{
				echo ']';
			}
			echo '\'>'.$key.'</li>';
		}
		echo 	'</ul>';
		echo '</span>';
	}

	public function base_shortcodes(){
		//Email Short Code
		$email_arg_defaults = array(
							"slug" => 'email',
							"attributes" => array('address' => ''),
							"description" => 'Creates a link to an email address.',
							"supports_content" => true,
							"sample_content" => 'Click Here',
						);
		// Allow the shortcode args to be overridden.
		$args = apply_filters('gecko/shortcode/email/args', $email_arg_defaults);
		$email_args = wp_parse_args($args, $email_arg_defaults); // This might be redundant.
		new \Gecko\Command\Shortcode($email_args, array($this , 'email_shortcode'));

		//Button Short Code
		$button_arg_defaults = array(
							"slug" => 'button',
							"attributes" => array('href' => '#', 'target' => ''),
							"description" => 'Creates a button styled link.',
							"supports_content" => true,
							"sample_content" => 'Click Here',
						);
		// Allow the shortcode args to be overridden.
		$args = apply_filters('gecko/shortcode/button/args', $button_arg_defaults);
		$button_args = wp_parse_args($args, $button_arg_defaults); // This might be redundant.
		new \Gecko\Command\Shortcode($button_args, array($this , 'button_shortcode'));
	}

	public function button_shortcode($atts, $content = null){
		// Check if the default adds are set here. Just in case the defaults are over ridden.
		$href = (isset($atts['href']))? esc_attr($atts['href']) : '';
		$target = (isset($atts['target']))? $atts['target'] : '';
		// Default html
		$html = '<a href="'. $href .'" target="'.$target.'" class="button">'.$content.'</a>';
		// Allow the shortcode html to be overridden.
		$html = apply_filters('gecko/shortcode/button/html', $html, $atts, $content);
		return $html;		
	}

	public function email_shortcode($atts, $content = null){
		// Check if the default adds are set here. Just in case the defaults are over ridden.
		$address = (isset($atts['address']))? esc_attr($atts['address']) : '';
		// Address is required but might should find a way to override it?
		if ( ! is_email( $address ) ) {
			return;
		}
		// Default html
		$html = '<a href="mailto:'.antispambot( $address ).'">'.$content.'</a>';
		// Allow the shortcode html to be overridden.
		$html = apply_filters('gecko/shortcode/email/html', $html, $atts, $content);
		return $html;
	}

}
