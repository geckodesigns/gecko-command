<?php
/**
 * GeckoCommand shortcode class.
 *
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */

namespace Gecko\Command; 


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Shortcode. This class is meant for extending.
 */
class Shortcode {
	private $args;
	private $callback;

	/*
	 * Constructor
	 */
	public function __construct($args = array("slug" => "", "description" => "", "supports_content" => false, "sample_content" => "", "attributes" => array()), $callback = null ){
		if($callback == null ){
			return;
		}
		$this->args = $args; //Should do a check for duplicate slugs
		$this->callback = $callback;
		add_shortcode( $this->args['slug'], array( $this, 'shortcode' ) );
		add_filter( 'gecko/register/shortcodes', array( $this, 'register' ));
	}

	/*
	 * Return Shortcode
	 */
	public function shortcode($atts, $content = null){
		$a = shortcode_atts($this->args['attributes'], $atts);
		return call_user_func($this->callback, $a, $content);
	}

	/*
	 * Register
	 */
	public function register($args){
		$args[$this->args['slug']] = $this->args;
		return $args;
	}

}
