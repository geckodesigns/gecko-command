<?php 
/**
 * Admin page foundation.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */
namespace Gecko\Command;


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Page Class.
 */
class AdminPage {

	public function __construct($function = ''){
		$this->header();
		$this->enqueue_scripts();
		add_filter( 'admin_footer_text', array( $this, 'footer_text' ), 10 );
		call_user_func($function);
	}

	/**
	 * Admin page header
	 */
	public static function header(){
		ob_start();
		?>
		<div class="gecko-dashboard__header">
			<div class="gecko-dashboard__logo"><img src="<?php echo GECKOCOMMAND__PLUGIN_URL; ?>assets/images/logo.png" height="100px"></div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		echo $html;
	} 
	
	/**
	 * Probably should load scripts and styles with this. But it doesn't work yet, so just calling wp_enqueue_style() in the __construct.
	 */
	public static function enqueue_scripts(){
		wp_enqueue_style( 'geckodesigns', GECKOCOMMAND__PLUGIN_URL.'assets/css/admin.css','',GECKOCOMMAND__VERSION);
		wp_enqueue_script( 'geckodesigns', GECKOCOMMAND__PLUGIN_URL.'assets/js/admin.js','',GECKOCOMMAND__VERSION);
	}

	/**
	 * Admin page foot text
	 */
	public static function footer_text(){
		$footer_text = sprintf( __( 'Our business is making your business look good. ~ %sGecko Designs%s', 'gecko-designs' ), '<a href="https://www.geckodesigns.com" target="_blank">', '</a>' );
		return $footer_text;
	}


}