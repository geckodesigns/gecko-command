<?php 
/**
 * Class for custom Gecko Designs login page.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */

namespace Gecko\Command; 

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Customize login page.
 */
class CustomLogin {

	/**
	 * Construct
	 */
	function __construct() {
		add_filter( 'login_headerurl', array( $this, 'headerurl'));
		add_filter( 'login_headertext', array( $this, 'headertitle'));
		add_action( 'login_enqueue_scripts', array( $this, 'enqueue_scripts'));
	}

	/**
	 * https://codex.wordpress.org/Plugin_API/Filter_Reference/login_headerurl/
	 */
	public function headerurl() {
		return "https://www.geckodesigns.com/";
	}

	/**
	 * https://developer.wordpress.org/reference/hooks/login_headertitle/
	 */
	public function headertitle() {
		return 'Build by Gecko Designs. Powered by Wordpress.';
	}

	/**
	 * https://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
	 */
	public function enqueue_scripts() {
		wp_enqueue_style("gecko-admin-styles", GECKOCOMMAND__PLUGIN_URL.'/assets/css/wp-login.css', [], GECKOCOMMAND__VERSION);
	}

}