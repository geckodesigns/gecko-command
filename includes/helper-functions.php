<?php
/*
 * Functions in here are simply for convinience(however you spell that)
 */

/*
 * Function for knowing if gecko command is installed.
 */
function gecko_command(){
	return true;
}

/*
 * Gecko Internal Notice Only
 * This is to prevent overriding Tres Commas Test Data
 */
function gecko_tres_commas_error() {
	$class = 'notice notice-error';
	$message = 'HEY!!!! You are editing data on Tres Commas! Don\'t add any images, but it would be better to update content <a href="http://192.168.1.251">there.</a>';
	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), $message); 
}

/*
 * Help with class name parsing
 */
function gecko_camel_to_dashed($className) {
    return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $className));
}

/*
 * Returns true if current user is a gecko user
 */
function is_gecko_user(){
	return \Gecko\Command\Users::is_user();
}