<?php
/**
 * Template Loader class.
 *
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */

namespace Gecko\Command; 


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Gecko Users class.
 */
class Users {
	
	/**
	 * Users Constructor.
	 */
	public function __construct() {
		// We need to run this only every now and then....Maybe on plugin update? Maybe check the api? Not sure.
		// $this->process_users();
		// add_action('gecko_command_plugin_updated', array( $this, 'process_users' ), 0 );
	}

	/**
	 * Returns an array of current gecko users and past with updated information
	 * May be better to add meta to gecko-users. Then use an api to add and update users.
	 */
	public static function get_users(){
		$users = array(
			'gabe' => array(
				'user_login' => 'gabe',
				'user_email' => 'gabe@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Gabe',
				'last_name' => 'Silverman',
				'nickname' => 'Gabe',
				'display_name' => 'Gabe',
			),
			'dylan' => array(
				'user_login' => 'dylan',
				'user_email' => 'dylan.pollard@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Dylan',
				'last_name' => 'Pollard',
				'nickname' => 'Dylan',
				'display_name' => 'Dylan',
			),
			'kevan' => array(
				'user_login' => 'kevan',
				'user_email' => 'kevan.slyngstad@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Kevan',
				'last_name' => 'Slyngstad',
				'nickname' => 'Kevan',
				'display_name' => 'Kevan',
			),
			'dwayne' => array(
				'user_login' => 'dwayne',
				'user_email' => 'dwayne.parton@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Dwayne',
				'last_name' => 'Parton',
				'nickname' => 'Dwayne',
				'display_name' => 'Dwayne',
			),
			'ali' => array(
				'user_login' => 'ali',
				'user_email' => 'ali.spoon@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Ali',
				'last_name' => 'Spoon',
				'nickname' => 'Ali',
				'display_name' => 'Ali',
			),
			'grace' => array(
				'user_login' => 'grace',
				'user_email' => 'grace.plant@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Grace',
				'last_name' => 'Plant',
				'nickname' => 'Grace',
				'display_name' => 'Grace',
			),
			'blake' => array(
				'user_login' => 'blake',
				'user_email' => 'blake.morton@geckodesigns.com',
				'role' => 'administrator',
				'create' => true,
				'first_name' => 'Blake',
				'last_name' => 'Morton',
				'nickname' => 'Blake',
				'display_name' => 'Blake',
			),
			'marshall' => array(
				'user_login' => 'marshall',
				'user_email' => 'marshall.hibbard@geckodesigns.com',
				'role' => 'administrator',
				'create' => false,
				'first_name' => 'Marshall',
				'last_name' => 'Hibbard',
				'nickname' => 'Marshall',
				'display_name' => 'Marshall',
			),
			'lucien' => array(
				'user_login' => 'lucien',
				'user_email' => 'lucien.greathouse@geckodesigns.com',
				'role' => 'subscriber',
				'create' => false,
				'first_name' => 'Lucien',
				'last_name' => 'Greathouse',
				'nickname' => 'Lucien',
				'display_name' => 'Lucien',
			),
			// 'tim' => array(
			// 	'user_login' => 'tim',
			// 	'user_email' => 'tim.friendson@geckodesigns.com',
			// 	'role' => 'subscriber',
			// 	'create' => false,
			// 	'first_name' => 'Tim',
			// 	'last_name' => 'Friendson',
			// 	'nickname' => 'Tim',
			// 	'display_name' => 'Tim',
			// ),
			'aemil' => array(
				'user_login' => 'aemil',
				'user_email' => 'aemil.estvold@geckodesigns.com',
				'role' => 'subscriber',
				'create' => false,
				'first_name' => 'Aemil',
				'last_name' => 'Estvold',
				'nickname' => 'Aemil',
				'display_name' => 'Aemil',
			),
			'tim' => array(
				'user_login' => 'tim',
				'user_email' => 'tim.hayes@geckodesigns.com',
				'role' => 'administrator',
				'create' => false,
				'first_name' => 'Tim',
				'last_name' => 'Hayes',
				'nickname' => 'Tim',
				'display_name' => 'Tim',
			),
		);
		return $users;
	}

	// Process all gecko users
	public function process_users(){
		// Get users here. Currently it's just an array
		$users = self::get_users();
		// Loop through users and process them
		foreach($users as $userdata){
			// Process User Data
			self::process_user($userdata);
		}
	}

	// Add the user 
	public static function process_user($userdata = array()){
		$user = get_user_by('login', $userdata['user_login']);
		// Default Gecko User Data
		$userdata['user_url'] = 'https://geckodesigns.com';
		// If the user doesn't exist and the email isn't registered
		if ( !$user && email_exists($userdata['user_email']) == false && $userdata['create']) {
			// Generate a strong password. The user can reset it if need be.
			$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			// Set the userdata password for wp_insert_user
			$userdata['user_pass'] = $random_password;
			// Add the user, returns the id
			$user_id = wp_insert_user( $userdata ) ;
			// On success
			if ( ! is_wp_error( $user_id ) ) {
				//echo "User created : ". $user_id;
			}
		// If User exists and the email matches then update the data but keep the same password
		// This is to make sure we don't override a potential user who took our default name
		}elseif($user && $user->user_email == $userdata['user_email']){
			// Update the user data if not super admin...don't wanna break anything if the site is a network. There might be a better solution for that.
			if ( !is_super_admin() ) {
				$user_id = wp_update_user( $userdata );
			}
		}else{
			//do nothing
		}
	}

	// Check if is gecko user.
	public static function is_user($user = null){
		if(!is_user_logged_in()){
			return false;
		}
		if($user == null){
			$user = wp_get_current_user();
		}
		$gecko_users = self::get_users();
		if(strpos($user->user_email, 'geckodesigns.com') !== false ){
			return true;
		}
		return false;
	}

}
