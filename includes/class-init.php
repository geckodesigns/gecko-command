<?php
/**
 * Initiate GeckoCommand.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */
namespace Gecko\Command; 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Main Gecko/Command Class.
 */
class Init {
	
	/**
	 * GeckoCommand Constructor.
	 */
	public function __construct() {
		add_action('init', array( $this, 'init' ), 0 );
	}
	
	/*
	 * Initialize the plugin
	 */
	public function init(){
		/*
		 * Get the plugin data so we don't have to remember to update the version
		 * $plugin_data = get_plugin_data(GECKOCOMMAND__FILE);
		 * Doesn't always work for some reason
		 */
		$plugin_data = get_file_data(GECKOCOMMAND__FILE, array('Version' => 'Version'));
		define( 'GECKOCOMMAND__VERSION', $plugin_data['Version']);

		/*
		 * Install ACF
		 */
		include_once( GECKOCOMMAND__PLUGIN_DIR. 'lib/advanced-custom-fields-pro/acf.php' );
		add_filter('acf/settings/path', array($this,'acf_settings_path'));
		add_filter('acf/settings/dir', array($this,'acf_settings_dir'));
		add_filter('acf/settings/show_admin', '__return_false');
		add_filter('acf/settings/capability', '__return_false');

		/*
		 * If Gecko user show ACF
		 */
		if(is_admin() && is_gecko_user() ){
			remove_filter( 'acf/settings/show_admin', '__return_false');
			remove_filter( 'acf/settings/capability', '__return_false');
		}

		/*
		 * Install Dashboard Components only for administrators and on dashboard
		 */
		if(is_admin() && current_user_can('administrator')){
			/*
			 * Install Admin Pages
			 * To uninstall use: add_filter('gecko/settings/install_admin', '__return_false');
			 */
			$install_admin = apply_filters('gecko/settings/install_admin', '__return_true');
			if($install_admin){
				new \Gecko\Command\Admin();
			}
			new \Gecko\Command\Updater(GECKOCOMMAND__FILE, 'geckodesigns', 'gecko-command');
		}

		/*
		 * Install Shortcodes
		 * To uninstall use: add_filter('gecko/settings/install_shortcodes', '__return_false');
		 */
		$install_shortcodes = apply_filters('gecko/settings/install_shortcodes', '__return_true');
		if($install_shortcodes && !get_field('gecko_uninstall_shortcodes','options')){
			new \Gecko\Command\Shortcodes();
		}

		/*
		 * Install CustomLogin
		 * To uninstall use: add_filter('gecko/settings/install_custom_login', '__return_false');
		 */
		$install_custom_login = apply_filters('gecko/settings/install_custom_login', '__return_true');
		if($install_shortcodes && !get_field('gecko_uninstall_custom_login','options')){
			new \Gecko\Command\CustomLogin();
		}

		/*
		 * Action for custom gecko plugin installation
		 */
		do_action( 'gecko/register/plugin'); 

		/*
		 * Set permissions for non gecko users
		 * You can test this function by removing '!is_gecko_user() &&'
		 */
		if(!is_gecko_user() && current_user_can('administrator')){
			add_filter('map_meta_cap', array($this, 'map_meta_cap'), 10, 4);
		}
	}

	/*
	 * Set ACF Directory
	 */
	public function acf_settings_dir($dir) {
		$dir = GECKOCOMMAND__PLUGIN_URL.'lib/advanced-custom-fields-pro/';
		return $dir;
	}

	/*
	 * Set ACF Path
	 */
	public function acf_settings_path($path) {
		$path = GECKOCOMMAND__PLUGIN_DIR.'lib/advanced-custom-fields-pro/';
		return $path;
	}

	/*
	 * Gecko Permissions
	 * For non-gecko administrators
	 * We use the map_meta_cap hook so that we don't delete the original permissions, but instead override them.
	 * https://codex.wordpress.org/Roles_and_Capabilities
	 * https://developer.wordpress.org/reference/hooks/map_meta_cap/
	 */
	public function map_meta_cap($caps, $cap, $user_id, $args) {
		$deny = array();
		// Permissions returns an array of capabilities that should be disallowed
		// These are pulled from ACF fields on Gecko Command Settings page
		$permissions = get_field('gecko_permissions','options');
		if($permissions):
			foreach ($permissions as $permission) {
				$deny[] = $permission;
			}
		endif;
		if (in_array($cap, $deny)) {
			$caps[] = 'do_not_allow';
		}
		return $caps;
	}
}
