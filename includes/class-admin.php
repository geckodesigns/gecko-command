<?php 
/**
 * Initiate the school athletics admin. It all starts here.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  Gecko\Command
 * @version  0.0.1
 */
namespace Gecko\Command;


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Admin class.
 */
class Admin {

	/**
	 * Hook in methods.
	 */
	public function __construct(){
		add_action( 'admin_menu', array( $this, 'register_menus' ), 5 );
		if(is_gecko_user()){
			self::settings_page();
			self::register_fields();
		}
	}

	/**
	 * Register admin menus.
	 * We have this function because some pages we show are custom and can't be built with acf :)
	 */
	public function register_menus(){
		// Only install the pages if is_gecko_user and the dashboard is installed.
		if(is_gecko_user() || !get_field('gecko_uninstall_dashboard','options')){
			add_menu_page( 'Gecko Designs', 'Gecko Designs', 'manage_options', 'gecko-command', array( $this, 'dashboard_page' ), GECKOCOMMAND__PLUGIN_URL.'/assets/images/icon.svg', 0 );
			add_submenu_page('gecko-command', 'Dashboard', 'Dashboard','manage_options', 'gecko-command' , array( $this, 'dashboard_page' ));
		}
	}

	/**
	 * Create ACF Settings Page
	 * Because our plugin depends on GeckoCommand we can use all acf functions :)
	 */
	public static function settings_page() {
		if( function_exists('acf_add_options_page')) {
			// add sub page
			acf_add_options_page(array(
				'page_title' 	=> 'Gecko Command Settings',
				'menu_title' 	=> 'Settings',
				'parent_slug' 	=> 'gecko-command',
				'menu_slug' => 'gecko-command-settings',
				'update_button'		=> __('Save', 'gecko-command'),
				'updated_message'	=> __('Settings updates', 'gecko-command'),
			));
			
		}
	}

	/**
	 * Add ACF Fields
	 * https://www.advancedcustomfields.com/resources/register-fields-via-php/
	 * The easiest way is to use the gui and then export group via acf in php form
	 */
	public static function register_fields() {
		if( function_exists('acf_add_local_field_group') ):
			/*
			 * Settings ACF Group
			 */
			acf_add_local_field_group(array(
				'key' => 'group_5b08523bdf7f0',
				'title' => 'Gecko Command : Settings',
				'fields' => array(
					array(
						'key' => 'field_5b085243673ad',
						'label' => 'Custom Login',
						'name' => 'gecko_uninstall_custom_login',
						'type' => 'true_false',
						'instructions' => 'Controls installation of Gecko Designs Custom Login page.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'ui' => 1,
						'ui_on_text' => 'Uninstalled',
						'ui_off_text' => 'Installed',
					),
					array(
						'key' => 'field_5b0854d211ecd',
						'label' => 'Shortcodes',
						'name' => 'gecko_uninstall_shortcodes',
						'type' => 'true_false',
						'instructions' => 'Controls installation of all Gecko Shortcodes.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'ui' => 1,
						'ui_on_text' => 'Uninstalled',
						'ui_off_text' => 'Installed',
					),
					array(
						'key' => 'field_5b0854d211ecf',
						'label' => 'Dashboard',
						'name' => 'gecko_uninstall_dashboard',
						'type' => 'true_false',
						'instructions' => 'Controls installation of Gecko Dashboard for clients.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
						'ui' => 1,
						'ui_on_text' => 'Uninstalled',
						'ui_off_text' => 'Installed',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'gecko-command-settings',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'field',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

			/*
			 * Permissions ACF Group
			 */
			acf_add_local_field_group(array(
				'key' => 'group_5b16a419760ae',
				'title' => 'Gecko Command : Permissions',
				'fields' => array(
					array(
						'key' => 'field_5b16a42326251',
						'label' => 'Permissions',
						'name' => 'gecko_permissions',
						'type' => 'checkbox',
						'instructions' => 'Use the check boxes to change permissions for non-gecko users.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array(
							'install_plugins' => 'Disallow Plugin Installation',
							'upload_plugins' => 'Disallow Plugin Upload',
							'update_plugins' => 'Disallow Plugin Updates',
							'edit_plugins' => 'Disallow Plugin Editing',
							'delete_plugins' => 'Disallow Plugin Deletion',
							'install_themes' => 'Disallow Theme Installation',
							'upload_themes' => 'Disallow Theme Upload',
							'update_themes' => 'Disallow Theme Updates',
							'delete_themes' => 'Disallow Theme Deletion',
							'edit_themes' => 'Disallow Theme Editing',
							'switch_themes' => 'Disallow Theme Switching',
							'edit_files' => 'Disallow File Editing',
							'export' => 'Disallow Wordpress Export',
							'update_core' => 'Disallow Wordpress Core Updates',
						),
						'allow_custom' => 0,
						'save_custom' => 0,
						'default_value' => array(
						),
						'layout' => 'vertical',
						'toggle' => 0,
						'return_format' => 'value',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'gecko-command-settings',
						),
					),
				),
				'menu_order' => 2,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => 1,
				'description' => '',
			));

		endif;
	}

	/**
	 * Create dashboard page.
	 */
	public function dashboard_page() {
		new \Gecko\Command\AdminPage(array( $this, 'dashboard_page_html' ));
	}

	/**
	 * Dashboard page html
	 */
	public function dashboard_page_html() {
		require_once GECKOCOMMAND__PLUGIN_DIR.'lib/parsedown/Parsedown.php';
		$Parsedown = new \Parsedown();
		ob_start();
		?>
		<div class="wrap gecko-dashboard">
			<h1 ><?php _e( 'Gecko Designs', 'gecko-command' ); ?></h1>
			<div class="gecko-dashboard__boxes">
				<div class="gecko-dashboard__box gecko-dashboard__box--main">
					<?php 
						if(file_exists (get_template_directory().'/README.md')){
							$theme_markdown = file_get_contents(get_template_directory().'/README.md');
							echo $Parsedown->text($theme_markdown);
						}
					?>
				</div>

				<div class="gecko-dashboard__box gecko-dashboard__box--sidebar">
					<h2><?php _e('Available Shortcodes', 'gecko-command' ); ?></h2>
					<p>Below are the Shortcodes provided by Gecko. These can be added by clicking the shortcodes button above the editor.</p>
					<hr />
					<ul>
					<?php 
					$shortcodes = \Gecko\Command\Shortcodes::get_shortcodes();
					foreach ($shortcodes as $key => $info) {
						echo '<li><strong>['.$key;
						foreach ($info['attributes'] as $att => $value) {
							echo ' '.$att.'="'.$value.'"';
						}
						if($info['supports_content']){
							echo ']'.$info['sample_content'].'[/'.$key.']';
						}else{
							echo ']';
						}
						echo '</strong><br /><em>'.$info['description'].'</em></li>';
					}
					?>
					</ul>
				</div>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		echo $html;
	}

}
