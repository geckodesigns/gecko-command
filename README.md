# Gecko Command #

Welcome to Gecko Command. This is where all the Gecko nerd stuff happens. Stay tuned because there's more to come!


## Hooks ##

gecko/register/plugin
gecko/register/shortcode

## Filters ##

gecko/settings/install_shortcodes
gecko/settings/install_customlogin
gecko/settings/install_admin
gecko/shortcode/button/args
gecko/shortcode/button/html
gecko/shortcode/email/args
gecko/shortcode/email/html

---

## Changelog ##

- 0.8.8 - Viewable in dashboard by admins
- 0.0.1 - the first release.