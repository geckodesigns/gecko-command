<?php
/**
 * Plugin Name: Gecko Command
 * Plugin URI: https://bitbucket.org/geckodesigns/gecko-command/wiki/Home
 * Description: Our business is making your business look good.
 * Version: 1.4.5
 * Author: Gecko Designs
 * Author URI: https://geckodesigns.com
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 * Text Domain: gecko-command
 * Domain Path: 
 *
 * @package GeckoCommand
 * @category Core
 * @author Dwayne Parton
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
define( 'GECKOCOMMAND__FILE', __FILE__);
define( 'GECKOCOMMAND__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'GECKOCOMMAND__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'GECKOCOMMAND__LIB', plugin_dir_path( __FILE__ ).'lib/' );

/*
 * Include Helper Functions
 * These need to be kept at a minimum but the goal is to provide some cross plugin functions for interacting with Gecko Command
 */
include_once( GECKOCOMMAND__PLUGIN_DIR. 'includes/helper-functions.php' );

/*
 * Check if Tres Commas for development purposes
 */
if(DB_NAME == 'tres-commas' && $_SERVER['HTTP_HOST'] == 'gecko-skeleton.gecko'){
	add_action( 'admin_notices', 'gecko_tres_commas_error' );
}

/*
 * Class AutoLoader
 * This is the one of the most important features of gecko command
 * It creates the naming convention for gecko plugins
 */
spl_autoload_register(function( $class) {
	$namespaces = explode('\\', $class);
	// Registering Additional Plugins Schema
	if(in_array('Gecko', $namespaces)){
		foreach ($namespaces as $namespace) {
			//Array ( [0] => Gecko [1] => Command [2] => ClassName ) 
			$file = $namespace;
		}
		$class_file = dirname(__DIR__).'/gecko-'.gecko_camel_to_dashed($namespaces[1]) . '/includes/class-'. gecko_camel_to_dashed($file)  . '.php';
		require_once $class_file;
	}
});

/*
 * Crank her up
 */
new Gecko\Command\Init();