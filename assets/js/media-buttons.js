jQuery(document).ready(function($){
	setupGeckoShortcodes();

	// acf.add_action('load', function( $el ){
	acf.add_action('wysiwyg_tinymce_init', function( ed, id, mceInit, $field ){
		var button = $field.find('.button');
		var shortcodeList = $field.find('ul');

		button.on('click', function(e){
			e.preventDefault();
			toggleList(shortcodeList);
		});

		shortcodeList.on('click', '[data-gecko-shortcode]', function(e){
			e.preventDefault();

			// add the attribute value to the wysiwyg editor
			var content = jQuery(this).attr('data-gecko-shortcode');
			wp.media.editor.insert(content);

			// hide the menu
			shortcodeList.removeAttr('data-status');
		});
	});

	function setupGeckoShortcodes() {
		jQuery('#wpcontent .gecko-shortcodes-wrapper').each(function(i, el) {
			var button = jQuery(el).find('.button');
			var shortcodeList = jQuery(el).find('ul');

			button.on('click', function(e){
				e.preventDefault();
				toggleList(shortcodeList);
			});

			shortcodeList.on('click', '[data-gecko-shortcode]', function(e){
				e.preventDefault();

				// add the attribute value to the wysiwyg editor
				var content = jQuery(this).attr('data-gecko-shortcode');
				wp.media.editor.insert(content);

				// hide the menu
				shortcodeList.removeAttr('data-status');
			});
		});
	}

	function toggleList(list) {
		// toggle the shortcode selection menu
		if (list.attr('data-status') === 'active') {
			list.removeAttr('data-status');
		} else {
			list.attr('data-status', 'active');
		}
	}
});